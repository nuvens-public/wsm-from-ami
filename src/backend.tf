##################################################################################
# BACKEND
##################################################################################

##################################################################################
# S3 BACKEND
##################################################################################
## Account ID: XX00YY11ZZ22
## Account Name: Customer Name
## Partition Key for DynamoDB = LockID

#terraform {
#  backend "s3" {
#    bucket         = "tfstate-XX00YY11ZZ22"
#    key            = "ec2-wsm/terraform.tfstate"
#    region         = "eu-west-2"
#    encrypt        = true
#    dynamodb_table = "tfstate-locking"
#  }
#}

##################################################################################
# GITLAB BACKEND
##################################################################################
terraform {
  backend "http" {
    address         = "https://gitlab.com/api/v4/projects/30000000/terraform/state/tfstate"
    lock_address    = "https://gitlab.com/api/v4/projects/30000000/terraform/state/tfstate/lock"
    unlock_address  = "https://gitlab.com/api/v4/projects/30000000/terraform/state/tfstate/lock"
  }
}

##################################################################################
# DATA
##################################################################################
data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_kms_alias" "kms_keys" {
  name = "alias/ENCRYPTION-KEYS-1"
}

#data "template_file" "init" { template = "${file("init.sh")}" }
