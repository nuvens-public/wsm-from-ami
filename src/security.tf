##################################################################################
# SECURITY GROUPS
##################################################################################

resource "aws_security_group" "sg_wsm" {
  name        = "SG-WSM-${var.projectname}"
  description = "Security Group for WSM Appliance"
  vpc_id      = var.vpc_id
     ingress {
       description = "Allow HTTP"
       from_port   = 80
       to_port     = 80
       protocol    = "tcp"
       cidr_blocks = [var.ingress_cidr_blocks]
     }
     ingress {
       description = "Internal Network"
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = [var.ingress_cidr_blocks]
     }
     egress {
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = [var.egress_cidr_blocks]
     }
  tags = {
    Name        = "SG-WSM-${var.projectname}"
    Environment = var.environment
    Project     = var.projectname
    Team        = var.team
  }
}