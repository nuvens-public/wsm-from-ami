##################################################################################
# EC2
##################################################################################
resource "random_shuffle" "priv_subnet" {
  input        = [var.subnet_a, var.subnet_b, var.subnet_c]
  result_count = 1
}

resource "random_shuffle" "pub_subnet" {
  input        = [var.dmz_a, var.dmz_b, var.dmz_c]
  result_count = 1
}

locals {
  priv_subnet_name   = random_shuffle.priv_subnet.result[0]
  priv_subnet_random = random_shuffle.priv_subnet.result[0]
  subnet_name        = random_shuffle.pub_subnet.result[0]
  subnet_random      = random_shuffle.pub_subnet.result[0]
}

resource "aws_instance" "ec2_wsm" {
  ami                  = var.ami_wsm
  key_name             = var.keypair_name
  instance_type        = var.type_wsm
  subnet_id            = local.priv_subnet_random
  iam_instance_profile = var.ssm_name
  ebs_optimized        = true
#  user_data            = data.template_file.init.rendered

  root_block_device {
    volume_type = "gp3"
    volume_size = 10
    iops        = 3000
    throughput  = 125
    encrypted   = true
    kms_key_id  = var.kms_keys_name
  }

  monitoring                  = false
  vpc_security_group_ids      = [aws_security_group.sg_wsm.id]
  associate_public_ip_address = false
  tags = {
    Name        = "EC2-WSM-${var.projectname}"
    Environment = var.environment
    Project     = var.projectname
    Team        = var.team
  }
}