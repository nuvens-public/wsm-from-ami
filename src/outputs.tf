##################################################################################
# OUTPUTS
##################################################################################
output "sec_group" {
  description = "Security Group ID"
  value       = aws_security_group.sg_wsm.id
}

output "sec_group_name" {
  description = "Security Group Name"
  value       = aws_security_group.sg_wsm.name
}

output "ec2_instance_private" {
  description = "EC2 Instance Private IP"
  value       = aws_instance.ec2_wsm.private_ip
}