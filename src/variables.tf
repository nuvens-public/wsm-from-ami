##################################################################################
# VARIABLES GLOBAL
##################################################################################
variable "region" {
  description = "AWS Region to use"
  default     = "eu-west-2"
}

variable "projectname" {
  default = "CUSTOMER"
}

variable "environment" {
  default = "NON-PRODUCTION"
}

variable "team" {
  default = "DevOps"
}

##################################################################################
# VARIABLES VPC
##################################################################################

variable "vpc_id" {
  default = "vpc-11b11f1a"
}

variable "cidr_block" {
  default = "172.31.0.0/16"
}

variable "ingress_cidr_blocks" {
  default = "172.31.0.0/16"
}

variable "egress_cidr_blocks" {
  default = "0.0.0.0/0"
}

variable "subnet_a" {
  default = "subnet-aa11a111"
}

variable "subnet_b" {
  default = "subnet-a11a1aaa"
}

variable "subnet_c" {
  default = "subnet-1aaaaaa1"
}

variable "dmz_a" {
  default = "subnet-aa11a111"
}

variable "dmz_b" {
  default = "subnet-a11a1aa1"
}

variable "dmz_c" {
  default = "subnet-1aaaaa11"
}

##################################################################################
# VARIABLES EC2
##################################################################################
# With x86 Processors Instances
variable "ami_wsm" {
  default = "ami-098d4339ecbb4997c"
}

variable "type_wsm" {
  default = "t3.large"
}

##################################################################################
# VARIABLES ENCRYPTION and KEYPAIRS
##################################################################################
variable "keypair_name" {
  default = "CUSTOMER-KEYPAIR"
}

variable "kms_keys_name" {
  default = "arn:aws:kms:eu-west-2:111111111111:key/afdef1ce-0064-441d-b976-57aa185aecd0"
}

##################################################################################
# VARIABLES SESSION MANAGER Profile
##################################################################################
variable "ssm_name" {
  default = "EC2SSM"
}